import java.util.*;
import java.util.stream.Collectors;

class MediumComputer extends Computer {
	private ArrayList<String> possibleWords;
	private String[] specialWords = {"black", "fight", "jumpy", "drown", "vexes"};
	private final int wordLength = 5;
	private int specialWordIndex = 0;
}