import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

public class WordUtils {

    private ArrayList<String> words ;

    public WordUtils(String filename){

        words = new ArrayList<>() ;
        Scanner sc = null ;
        try {
            sc = new Scanner(new File(filename)) ;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while (sc.hasNextLine()){
            words.add(sc.nextLine()) ;
        }

    }

    public ArrayList<String> getWordsWithNonRepLetters(int length){

        return (ArrayList<String>)words.stream()
                .filter(s -> (s.length()==length)&&(unique(s)==s.length()))
                .collect(Collectors.toList()) ;

    }

    private int unique(String s){
        char[] arr = s.toCharArray() ;
        Set<Character> set = new HashSet<Character>();
        for (char ch : arr){
            set.add(ch) ;
        }
        return set.size() ;
    }





}
