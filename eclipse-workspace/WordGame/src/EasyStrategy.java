import java.util.*;
import java.util.stream.Collectors;

class EasyStrategy {
	private ArrayList<String> possibleWords;
	private String[] specialWords = {"myth", "duck", "verb", "gasp", "jinx", "wolf", "quit", "zone"};
	private final int wordLength = 4;
	private int specialWordIndex = 0;
	
	public String guessWord() {
		String guessWord = "";
		if(specialWordIndex == specialWords.length-1) {
			Random randomizer = new Random();
			guessWord = possibleWords.get(randomizer.nextInt(possibleWords.size()));
		}
		else
			guessWord = specialWords[specialWordIndex++];

		int sim = Game.getSim(guessWord) ; 
		possibleWords = (ArrayList<String>)possibleWords.stream()
		        .filter(w->toRemove(w,guessWord,sim))
		        .collect(Collectors.toList()) ; 
		
		return guessWord;
	}

	
	private boolean toRemove(String guessedWord, String filterWord, int actualSim) {
		int currentSim = Similarity.similarity(guessedWord, filterWord);
		return currentSim != actualSim;
	}
}


