import java.util.*;

class Similarity {
	
	
	static public int sim(String w1, String w2){
		HashSet<Character> h1 = new HashSet<Character>(), h2 = new HashSet<Character>();
		for(int i = 0; i < w1.length(); i++)                                            
		  h1.add(w1.charAt(i));
		  
		for(int i = 0; i < w2.length(); i++)
		  h2.add(w2.charAt(i));
		h1.retainAll(h2);
		
		return h1.size();
	}
	
}